let color = document.querySelector('#color')
let resultDiv = document.querySelector('#resultDiv')

color.addEventListener('input', (e) => {
    resultDiv.style.backgroundColor = e.target.value;
    resultDiv.innerHTML = e.target.value;
})