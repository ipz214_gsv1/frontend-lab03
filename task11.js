let inner = document.querySelector('#inner')
let addButton = document.querySelector('#addButton')

addButton.addEventListener('click', () => {
    let currentWidth = inner.offsetWidth;

    if (currentWidth == 100) {
        inner.style.width = `0px`
    }
    else {

        inner.style.width = `${currentWidth + 5}px`;
    }
})