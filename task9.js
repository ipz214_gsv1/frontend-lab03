let okButton = document.querySelector('#okButton')
let textDiv = document.querySelector('#textDiv')

okButton.addEventListener('click', () => {
    if (textDiv.style.display == "none") {
        textDiv.style.display = "block"
    }
    else {
        textDiv.style.display = "none"
    }
})