let sideAInput = document.querySelector('#sideA');
let sideBInput = document.querySelector('#sideB');
let okButton = document.querySelector('#okButton');
let perimeterResult = document.querySelector('#perimeterResult');
let squareResult = document.querySelector('#squareResult');


okButton.addEventListener('click', () => {
    let perimeter = (Number(sideAInput.value) * 2) + (Number(sideBInput.value) * 2);
    perimeterResult.innerHTML = `Периметр прямокутника дорівнює = ${perimeter} см`;
    let square = Number(sideAInput.value) * Number(sideBInput.value);
    squareResult.innerHTML = `Площа прямокутника дорівнює = ${square} см^2`;
})