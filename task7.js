let okButton = document.querySelector('#okButton')
let price = document.querySelector('#price')
let stage = document.querySelector('#stage')

okButton.addEventListener('click', (e) => {
    let values = document.getElementsByName('house')

    let currentPrice = Number(document.querySelector('#price').value)
    let currentStage = Number(document.querySelector('#stage').value)

    if (currentPrice >= 600000)
        console.log('Not interested!')

    if (currentStage > 6 || currentStage < 2)
        console.log('Not interested!')

    values.forEach(item => {
        if (item.checked) {
            if (item.value == 'vtorynne')
                console.log('Not interested!')
        }
    })
})