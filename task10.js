let html = document.querySelector('#html')
let css = document.querySelector('#css')
let js = document.querySelector('#js')

let htmlContent = document.querySelector('#htmlContent')
let cssContent = document.querySelector('#cssContent')
let jsContent = document.querySelector('#jsContent')



let currentMenuChoosen = htmlContent;
let currentItemChoosen = html;


html.addEventListener('click', () => {
    currentItemChoosen.style.backgroundColor = "aqua";
    currentMenuChoosen.style.display = "none";
    currentMenuChoosen = htmlContent;
    currentItemChoosen = html;
    currentMenuChoosen.style.display = "block";
    currentItemChoosen.style.backgroundColor = "#42d4f5";
})

css.addEventListener('click', () => {
    currentItemChoosen.style.backgroundColor = "aqua";
    currentMenuChoosen.style.display = "none";
    currentMenuChoosen = cssContent;
    currentItemChoosen = css;
    currentMenuChoosen.style.display = "block";
    currentItemChoosen.style.backgroundColor = "#42d4f5";
})
js.addEventListener('click', () => {
    currentItemChoosen.style.backgroundColor = "aqua";
    currentMenuChoosen.style.display = "none";
    currentMenuChoosen = jsContent;
    currentItemChoosen = js;
    currentMenuChoosen.style.display = "block";
    currentItemChoosen.style.backgroundColor = "#42d4f5";
})
