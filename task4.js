let seconds = document.querySelector('#seconds');
let mileage = document.querySelector('#mileage');
let okButton = document.querySelector('#okButton');
let resultDiv = document.querySelector('#resultDiv');


okButton.addEventListener('click', () => {
    let hours = Number(seconds.value) / 3600;

    let result = Number(mileage.value) / hours;

    resultDiv.innerHTML = `Швидкість руху \n ${result / 1000} км/год`;
})