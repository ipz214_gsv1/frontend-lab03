let firstInput = document.querySelector('#word1');
let secondInput = document.querySelector('#word2');
let okButton = document.querySelector('#okButton');


okButton.addEventListener('click', () => {
    let tmpValue = firstInput.value;
    firstInput.value = secondInput.value;
    secondInput.value = tmpValue;
})