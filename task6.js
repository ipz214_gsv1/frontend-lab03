let select = document.querySelector('#select')
let resultDiv = document.querySelector('#resultDiv')


select.addEventListener('change', (e) => {
    switch (Number(e.target.value)) {
        case 1:
            resultDiv.src = './images/chicago.jpg'
            break;

        case 2:
            resultDiv.src = './images/los-angeles.jpg'
            break;

        case 3:
            resultDiv.src = './images/new-york.jpg'
            break;
        
        default:
            break;
    }
})